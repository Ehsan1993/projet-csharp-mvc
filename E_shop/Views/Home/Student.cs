﻿namespace MVCSchool.Views.Home
{
    public class Student
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public ICollection<Course> Courses { get;set; }
    }
}
