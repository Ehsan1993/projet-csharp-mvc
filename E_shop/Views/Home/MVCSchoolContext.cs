﻿using Microsoft.EntityFrameworkCore;

namespace MVCSchool.Views.Home
{
    public class MVCSchoolContext : DbContext
    {
        public MVCSchoolContext(DbContextOptions<MVCSchoolContext> options) 
        {
        
        }

        public DbSet<Student> Students { get; set; }

        public DbSet<Course> Courses { get; set; }
    }
}
